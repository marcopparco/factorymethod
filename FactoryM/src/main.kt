interface Producto {
    fun clima(): String;

}

class Zanahoria : Producto {
    override fun clima(): String {
        return "Frio"
    }
}


class Platano : Producto {
    override fun clima(): String {
        return "Caliente"
    }
}

enum class Region {
    Costa, Sierra, Amazonia
}

class methodFactory(){
    fun productos(region: Region): Producto {
        return when (region) {
            Region.Costa -> return Platano()
            Region.Amazonia  -> return Platano()
            Region.Sierra -> return Zanahoria()
            else -> throw RuntimeException("Unknown shape $region")
        }

    }
}


fun main(args: Array<String>) {

    println("********************------------*******************")
    val metodo= methodFactory()
    val producto1: Producto= metodo.productos(Region.Sierra)
    println(producto1.clima())
    val producto2: Producto= metodo.productos(Region.Amazonia)
    println(producto2.clima())
    val producto3: Producto= metodo.productos(Region.Costa)
    println(producto3.clima())
    val producto4: Producto= metodo.productos(Region.Sierra)
    println(producto4.clima())
}
